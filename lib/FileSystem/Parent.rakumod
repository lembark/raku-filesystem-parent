########################################################################
# housekeeping
########################################################################

use v6.d;

unit module FileSystem::Parent:ver<0.4.3>:auth<CPAN:lembark>;

use FindBin;

################################################################
# exported (API)
################################################################

multi scan-up
(
    Stringy :$filter = 'all',
    *%argz
    --> Seq
)
{
    my %filterz =
    (
        all     => True ,
        dir     => :d   ,
        file    => :f   ,
        exist   => :e   ,
    );

    my $value   = %filterz{ $filter }
    or
    fail "Unknown: '$filter'. not " ~ %filterz.keys.join(' ');

    samewith filter => $value, |%argz
}

multi scan-up
(
    Any     :$filter,

    Str     :$append    = '',
    Bool()  :$resolve   = False,
    Bool()  :$verbose   = False,
    Bool()  :$skip-root = False,
    IO()    :$from      = Bin( :$resolve, :$verbose )
    --> Seq
)
is export( :DEFAULT )
{
    # resolve is only applied once, here.

    my $start    = 
    (
        $resolve 
        ?? $from.resolve( completely => True ) 
        !! $from
    )
    .absolute.IO;

    # note that $start may have nothing to do with $from
    # at this point if $resolve is used w/ a symlink. 

    my $path
    = $start ~~ :d
    ?? $start
    !! $start.parent
    ;

    if $verbose
    {
        note '# From:   ' ~ $from.gist;
        note '# Start:  ' ~ $start.gist;
        note "# Path:   '$path'";
        note "# Append: '$append'";
    }

    return gather loop
    {
        # $parent & $path may contain a volume or 
        # path trim, compare all of it.

        my $next    = $path.parent;
        my $at-root = $path ~~ $next;

        my $take
        = $append
        ?? $path.add( $append )
        !! $path
        ;

        # make the check before append and take in 

        if $at-root && $skip-root
        {
            note "# Root: '$take' (skip)"
            if $verbose;
        }
        else
        {
            # $take might be a file,
            # depending on what's in $append.
            #
            # resolve is *not* applied
            # at each iteration.

            if $take ~~ $filter
            {
                note "# Want: '$take'"
                if $verbose;

                take $take;
            }
            else
            {
                note "# Skip: '$take'"
                if $verbose;
            }
        };

        $at-root
        and last;

        $path   = $next;
    }
}

=finish
